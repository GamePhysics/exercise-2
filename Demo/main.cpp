//--------------------------------------------------------------------------------------
// File: main.cpp
//
// The main file containing the entry point main().
//--------------------------------------------------------------------------------------

#include <sstream>
#include <iomanip>
#include <random>
#include <iostream>

//DirectX includes
#include <DirectXMath.h>
using namespace DirectX;

// Effect framework includes
#include <d3dx11effect.h>

// DXUT includes
#include <DXUT.h>
#include <DXUTcamera.h>

// DirectXTK includes
#include "Effects.h"
#include "VertexTypes.h"
#include "PrimitiveBatch.h"
#include "GeometricPrimitive.h"
#include "ScreenGrab.h"

// AntTweakBar includes
#include "AntTweakBar.h"

// Internal includes
#include "util/util.h"
#include "util/FFmpeg.h"


#include <vector>
#include <set>
#include "RigidBody.h"
#include "collisionDetect.h"

// DXUT camera
// NOTE: CModelViewerCamera does not only manage the standard view transformation/camera position 
//       (CModelViewerCamera::GetViewMatrix()), but also allows for model rotation
//       (CModelViewerCamera::GetWorldMatrix()). 
//       Look out for CModelViewerCamera::SetButtonMasks(...).
CModelViewerCamera g_camera;

// Effect corresponding to "effect.fx"
ID3DX11Effect* g_pEffect = nullptr;

// Main tweak bar
TwBar* g_pTweakBar;

// DirectXTK effects, input layouts and primitive batches for different vertex types
BasicEffect*                               g_pEffectPositionColor               = nullptr;
ID3D11InputLayout*                         g_pInputLayoutPositionColor          = nullptr;
PrimitiveBatch<VertexPositionColor>*       g_pPrimitiveBatchPositionColor       = nullptr;
                                                                                
BasicEffect*                               g_pEffectPositionNormal              = nullptr;
ID3D11InputLayout*                         g_pInputLayoutPositionNormal         = nullptr;
PrimitiveBatch<VertexPositionNormal>*      g_pPrimitiveBatchPositionNormal      = nullptr;
                                                                                
BasicEffect*                               g_pEffectPositionNormalColor         = nullptr;
ID3D11InputLayout*                         g_pInputLayoutPositionNormalColor    = nullptr;
PrimitiveBatch<VertexPositionNormalColor>* g_pPrimitiveBatchPositionNormalColor = nullptr;

// DirectXTK simple geometric primitives
std::unique_ptr<GeometricPrimitive> g_pSphere;
std::unique_ptr<GeometricPrimitive> g_pCube;
std::unique_ptr<GeometricPrimitive> g_pTeapot;

// Movable object management
XMINT2   g_viMouseDelta = XMINT2(0,0);
XMFLOAT3 g_vfMovableObjectPos = XMFLOAT3(0,0,0);

// TweakAntBar GUI variables
int   g_iNumSpheres    = 100;
float g_fSphereSize    = 0.05f;
bool  g_bDrawTeapot    = false;
bool  g_bDrawTriangle  = false;
bool  g_bDrawSpheres   = false;



// own variables:
float g_fGravity = 10.0f;
bool keyDownMap[256];
// rigid bodies:
std::vector<RigidBody> rigidBodies;


// Video recorder
FFmpeg* g_pFFmpegVideoRecorder = nullptr;


void initRigidBodies(){
	rigidBodies.clear();
	/*RigidBody eins(-1.0f, 1, 0, 0.15f, 0.25f, XMVectorSet(1.0f, 0.5f, 0.0f, 0.0f), XMVectorSet(0, 1, 0, 0), XM_PIDIV4);
	rigidBodies.push_back(eins);
	RigidBody zwei(0.0f, 1, 0, 0.1f, 0.45f, XMVectorSet(0.0f, 0.5f, 1.0f, 0.0f), XMVectorSet(1, 0, 0, 0), XM_PIDIV4);
	rigidBodies.push_back(zwei);
	RigidBody drei(1.0f, 1, 0, 0.05f, 0.6f, XMVectorSet(0.0f, 1.0f, 0.5f, 0.0f), XMVectorSet(0, 0, 1, 0), XM_PIDIV4);
	rigidBodies.push_back(drei);*/
	RigidBody einsUnrotiert(-1.0f, 1, 0, 0.15f, 0.25f, XMVectorSet(1.0f, 0.5f, 0.0f, 0.0f), XMVectorSet(0, 1, 0, 0), 0);
	rigidBodies.push_back(einsUnrotiert);
	RigidBody einsUnrotiert2(0.0f, 1, 0, 0.15f, 0.25f, XMVectorSet(0.0f, 0.5f, 1.0f, 0.0f), XMVectorSet(1, 0, 0, 0), 0);
	rigidBodies.push_back(einsUnrotiert2);
	RigidBody einsUnrotiert3(1.0f, 1, 0, 0.15f, 0.25f, XMVectorSet(0.0f, 1.0f, 0.5f, 0.0f), XMVectorSet(0, 0, 1, 0), 0);
	rigidBodies.push_back(einsUnrotiert3);
	/*RigidBody zweiUnrotiert(0.0f, 1, 0, 0.1f, 0.45f, XMVectorSet(0.0f, 0.5f, 1.0f, 0.0f), XMVectorSet(1, 0, 0, 0), 0);
	rigidBodies.push_back(zweiUnrotiert);
	RigidBody dreiUnrotiert(1.0f, 1, 0, 0.05f, 0.6f, XMVectorSet(0.0f, 1.0f, 0.5f, 0.0f), XMVectorSet(0, 0, 1, 0), 0);
	rigidBodies.push_back(dreiUnrotiert);*/
}

// Create TweakBar and add required buttons and variables
void InitTweakBar(ID3D11Device* pd3dDevice)
{
    TwInit(TW_DIRECT3D11, pd3dDevice);

    g_pTweakBar = TwNewBar("TweakBar");

    // HINT: For buttons you can directly pass the callback function as a lambda expression.
	TwAddButton(g_pTweakBar, "Reset Camera", [](void *){g_camera.Reset(); }, nullptr, "");

	TwAddButton(g_pTweakBar, "Reset RigidBodies", [](void *){initRigidBodies(); }, nullptr, "");
	TwAddVarRW(g_pTweakBar, "Gravity", TW_TYPE_FLOAT, &g_fGravity, "step=0.01");
	
    //TwAddVarRW(g_pTweakBar, "Draw Teapot",   TW_TYPE_BOOLCPP, &g_bDrawTeapot, "");
    //TwAddVarRW(g_pTweakBar, "Draw Triangle", TW_TYPE_BOOLCPP, &g_bDrawTriangle, "");
    //TwAddVarRW(g_pTweakBar, "Draw Spheres",  TW_TYPE_BOOLCPP, &g_bDrawSpheres, "");
    //TwAddVarRW(g_pTweakBar, "Num Spheres",   TW_TYPE_INT32, &g_iNumSpheres, "min=1");
    //TwAddVarRW(g_pTweakBar, "Sphere Size",   TW_TYPE_FLOAT, &g_fSphereSize, "min=0.01 step=0.01");
}

// Draw the edges of the bounding box [-0.5;0.5]� rotated with the cameras model tranformation.
// (Drawn as line primitives using a DirectXTK primitive batch)
void DrawBoundingBox(ID3D11DeviceContext* pd3dImmediateContext)
{
    // Setup position/color effect
    g_pEffectPositionColor->SetWorld(g_camera.GetWorldMatrix());
    
    g_pEffectPositionColor->Apply(pd3dImmediateContext);
    pd3dImmediateContext->IASetInputLayout(g_pInputLayoutPositionColor);

    // Draw
    g_pPrimitiveBatchPositionColor->Begin();
    
    // Lines in x direction (red color)
    for (int i=0; i<4; i++)
    {
        g_pPrimitiveBatchPositionColor->DrawLine(
            VertexPositionColor(XMVectorSet(-0.5f, (float)(i%2)-0.5f, (float)(i/2)-0.5f, 1), Colors::Red),
            VertexPositionColor(XMVectorSet( 0.5f, (float)(i%2)-0.5f, (float)(i/2)-0.5f, 1), Colors::Red)
        );
    }

    // Lines in y direction
    for (int i=0; i<4; i++)
    {
        g_pPrimitiveBatchPositionColor->DrawLine(
            VertexPositionColor(XMVectorSet((float)(i%2)-0.5f, -0.5f, (float)(i/2)-0.5f, 1), Colors::Green),
            VertexPositionColor(XMVectorSet((float)(i%2)-0.5f,  0.5f, (float)(i/2)-0.5f, 1), Colors::Green)
        );
    }

    // Lines in z direction
    for (int i=0; i<4; i++)
    {
        g_pPrimitiveBatchPositionColor->DrawLine(
            VertexPositionColor(XMVectorSet((float)(i%2)-0.5f, (float)(i/2)-0.5f, -0.5f, 1), Colors::Blue),
            VertexPositionColor(XMVectorSet((float)(i%2)-0.5f, (float)(i/2)-0.5f,  0.5f, 1), Colors::Blue)
        );
    }

    g_pPrimitiveBatchPositionColor->End();
}

// Draw a large, square plane at y=-1 with a checkerboard pattern
// (Drawn as multiple quads, i.e. triangle strips, using a DirectXTK primitive batch)
void DrawFloor(ID3D11DeviceContext* pd3dImmediateContext)
{
    // Setup position/normal/color effect
    g_pEffectPositionNormalColor->SetWorld(XMMatrixIdentity());
    g_pEffectPositionNormalColor->SetEmissiveColor(Colors::Black);
    g_pEffectPositionNormalColor->SetDiffuseColor(0.8f * Colors::White);
    g_pEffectPositionNormalColor->SetSpecularColor(0.4f * Colors::White);
    g_pEffectPositionNormalColor->SetSpecularPower(1000);

    g_pEffectPositionNormalColor->Apply(pd3dImmediateContext);
    pd3dImmediateContext->IASetInputLayout(g_pInputLayoutPositionNormalColor);

    // Draw 4*n*n quads spanning x = [-n;n], y = -1, z = [-n;n]
    const float n = 4;
    XMVECTOR normal      = XMVectorSet(0, 1,0,0);
    XMVECTOR planecenter = XMVectorSet(0,-1,0,0);

    g_pPrimitiveBatchPositionNormalColor->Begin();
    for (float z = -n; z < n; z++)
    {
        for (float x = -n; x < n; x++)
        {
            // Quad vertex positions
            XMVECTOR pos[] = { XMVectorSet(x  , -1, z+1, 0),
                               XMVectorSet(x+1, -1, z+1, 0),
                               XMVectorSet(x+1, -1, z  , 0),
                               XMVectorSet(x  , -1, z  , 0) };

            // Color checkerboard pattern (white & gray)
            XMVECTOR color = ((int(z + x) % 2) == 0) ? XMVectorSet(1,1,1,1) : XMVectorSet(0.6f,0.6f,0.6f,1);

            // Color attenuation based on distance to plane center
            float attenuation[] = {
                1.0f - XMVectorGetX(XMVector3Length(pos[0] - planecenter)) / n,
                1.0f - XMVectorGetX(XMVector3Length(pos[1] - planecenter)) / n,
                1.0f - XMVectorGetX(XMVector3Length(pos[2] - planecenter)) / n,
                1.0f - XMVectorGetX(XMVector3Length(pos[3] - planecenter)) / n };

            g_pPrimitiveBatchPositionNormalColor->DrawQuad(
                VertexPositionNormalColor(pos[0], normal, attenuation[0] * color),
                VertexPositionNormalColor(pos[1], normal, attenuation[1] * color),
                VertexPositionNormalColor(pos[2], normal, attenuation[2] * color),
                VertexPositionNormalColor(pos[3], normal, attenuation[3] * color)
            );
        }
    }
    g_pPrimitiveBatchPositionNormalColor->End();    
}

// Draw several objects randomly positioned in [-0.5f;0.5]�  using DirectXTK geometric primitives.
void DrawSomeRandomObjects(ID3D11DeviceContext* pd3dImmediateContext)
{
    // Setup position/normal effect (constant variables)
    g_pEffectPositionNormal->SetEmissiveColor(Colors::Black);
    g_pEffectPositionNormal->SetSpecularColor(0.4f * Colors::White);
    g_pEffectPositionNormal->SetSpecularPower(100);
      
    std::mt19937 eng;
    std::uniform_real_distribution<float> randCol( 0.0f, 1.0f);
    std::uniform_real_distribution<float> randPos(-0.5f, 0.5f);

    for (int i=0; i<g_iNumSpheres; i++)
    {
        // Setup position/normal effect (per object variables)
        g_pEffectPositionNormal->SetDiffuseColor(0.6f * XMColorHSVToRGB(XMVectorSet(randCol(eng), 1, 1, 0)));
        XMMATRIX scale    = XMMatrixScaling(g_fSphereSize, g_fSphereSize, g_fSphereSize);
        XMMATRIX trans    = XMMatrixTranslation(randPos(eng),randPos(eng),randPos(eng));
        g_pEffectPositionNormal->SetWorld(scale * trans * g_camera.GetWorldMatrix());

        // Draw
        // NOTE: The following generates one draw call per object, so performance will be bad for n>>1000 or so
        g_pSphere->Draw(g_pEffectPositionNormal, g_pInputLayoutPositionNormal);
    }
}

// Draw a teapot at the position g_vfMovableObjectPos.
void DrawMovableTeapot(ID3D11DeviceContext* pd3dImmediateContext)
{
    // Setup position/normal effect (constant variables)
    g_pEffectPositionNormal->SetEmissiveColor(Colors::Black);
    g_pEffectPositionNormal->SetDiffuseColor(0.6f * Colors::Cornsilk);
    g_pEffectPositionNormal->SetSpecularColor(0.4f * Colors::White);
    g_pEffectPositionNormal->SetSpecularPower(100);

    XMMATRIX scale    = XMMatrixScaling(0.5f, 0.5f, 0.5f);    
    XMMATRIX trans    = XMMatrixTranslation(g_vfMovableObjectPos.x, g_vfMovableObjectPos.y, g_vfMovableObjectPos.z);
    g_pEffectPositionNormal->SetWorld(scale * trans);

    // Draw
    g_pTeapot->Draw(g_pEffectPositionNormal, g_pInputLayoutPositionNormal);
}

// Draw a simple triangle using custom shaders (g_pEffect)
void DrawTriangle(ID3D11DeviceContext* pd3dImmediateContext)
{
	XMMATRIX world = g_camera.GetWorldMatrix();
	XMMATRIX view  = g_camera.GetViewMatrix();
	XMMATRIX proj  = g_camera.GetProjMatrix();
	XMFLOAT4X4 mViewProj;
	XMStoreFloat4x4(&mViewProj, world * view * proj);
	g_pEffect->GetVariableByName("g_worldViewProj")->AsMatrix()->SetMatrix((float*)mViewProj.m);
	g_pEffect->GetTechniqueByIndex(0)->GetPassByIndex(0)->Apply(0, pd3dImmediateContext);
    
	pd3dImmediateContext->IASetVertexBuffers(0, 0, nullptr, nullptr, nullptr);
	pd3dImmediateContext->IASetIndexBuffer(nullptr, DXGI_FORMAT_R16_UINT, 0);
	pd3dImmediateContext->IASetInputLayout(nullptr);
	pd3dImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	pd3dImmediateContext->Draw(3, 0);
}

void DrawRigidBodies(ID3D11DeviceContext* pd3dImmediateContext)
{
	// Setup position/normal effect (constant variables)
    g_pEffectPositionNormal->SetEmissiveColor(Colors::Black);
    g_pEffectPositionNormal->SetSpecularColor(0.4f * Colors::White);
    g_pEffectPositionNormal->SetSpecularPower(100);
     
	for(std::vector<RigidBody>::iterator it = rigidBodies.begin() ; it != rigidBodies.end(); it++){
        // Setup position/normal effect (per object variables)
		g_pEffectPositionNormal->SetDiffuseColor(it->color);// XMColorHSVToRGB(XMVectorSet(0.6f, 1.0f, 1.0f, 1)));
		if (it->selected){
			g_pEffectPositionNormal->SetAlpha(0.3f);
		}
        XMMATRIX scale    = XMMatrixScaling(it->seitenlaenge, it->seitenlaenge, it->seitenlaenge);
		XMMATRIX trans = XMLoadFloat4x4(&it->transform);
		g_pEffectPositionNormal->SetWorld(scale * trans * g_camera.GetWorldMatrix());

		// http://www.dogonfire.dk/3DTutorials/rotation.php

        // Draw
        // NOTE: The following generates one draw call per object, so performance will be bad for n>>1000 or so
        g_pCube->Draw(g_pEffectPositionNormal, g_pInputLayoutPositionNormal);
		g_pEffectPositionNormal->SetAlpha(1); // reset alpha value
    }
}



// ============================================================
// DXUT Callbacks
// ============================================================


//--------------------------------------------------------------------------------------
// Reject any D3D11 devices that aren't acceptable by returning false
//--------------------------------------------------------------------------------------
bool CALLBACK IsD3D11DeviceAcceptable( const CD3D11EnumAdapterInfo *AdapterInfo, UINT Output, const CD3D11EnumDeviceInfo *DeviceInfo,
                                       DXGI_FORMAT BackBufferFormat, bool bWindowed, void* pUserContext )
{
	return true;
}


//--------------------------------------------------------------------------------------
// Called right before creating a device, allowing the app to modify the device settings as needed
//--------------------------------------------------------------------------------------
bool CALLBACK ModifyDeviceSettings( DXUTDeviceSettings* pDeviceSettings, void* pUserContext )
{
	return true;
}


//--------------------------------------------------------------------------------------
// Create any D3D11 resources that aren't dependent on the back buffer
//--------------------------------------------------------------------------------------
HRESULT CALLBACK OnD3D11CreateDevice( ID3D11Device* pd3dDevice, const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc, void* pUserContext )
{
	HRESULT hr;

    ID3D11DeviceContext* pd3dImmediateContext = DXUTGetD3D11DeviceContext();;

    std::wcout << L"Device: " << DXUTGetDeviceStats() << std::endl;
    
    // Load custom effect from "effect.fxo" (compiled "effect.fx")
	std::wstring effectPath = GetExePath() + L"effect.fxo";
	if(FAILED(hr = D3DX11CreateEffectFromFile(effectPath.c_str(), 0, pd3dDevice, &g_pEffect)))
	{
        std::wcout << L"Failed creating effect with error code " << int(hr) << std::endl;
		return hr;
	}

    // Init AntTweakBar GUI
    InitTweakBar(pd3dDevice);

    // Create DirectXTK geometric primitives for later usage
    g_pSphere = GeometricPrimitive::CreateGeoSphere(pd3dImmediateContext, 2.0f, 2, false);
    g_pCube = GeometricPrimitive::CreateCube(pd3dImmediateContext, 2.0f, false);
    g_pTeapot = GeometricPrimitive::CreateTeapot(pd3dImmediateContext, 1.5f, 8, false);

    // Create effect, input layout and primitive batch for position/color vertices (DirectXTK)
    {
        // Effect
        g_pEffectPositionColor = new BasicEffect(pd3dDevice);
        g_pEffectPositionColor->SetVertexColorEnabled(true); // triggers usage of position/color vertices

        // Input layout
        void const* shaderByteCode;
        size_t byteCodeLength;
        g_pEffectPositionColor->GetVertexShaderBytecode(&shaderByteCode, &byteCodeLength);
        
        pd3dDevice->CreateInputLayout(VertexPositionColor::InputElements,
                                      VertexPositionColor::InputElementCount,
                                      shaderByteCode, byteCodeLength,
                                      &g_pInputLayoutPositionColor);

        // Primitive batch
        g_pPrimitiveBatchPositionColor = new PrimitiveBatch<VertexPositionColor>(pd3dImmediateContext);
    }

    // Create effect, input layout and primitive batch for position/normal vertices (DirectXTK)
    {
        // Effect
        g_pEffectPositionNormal = new BasicEffect(pd3dDevice);
        g_pEffectPositionNormal->EnableDefaultLighting(); // triggers usage of position/normal vertices
        g_pEffectPositionNormal->SetPerPixelLighting(true);

        // Input layout
        void const* shaderByteCode;
        size_t byteCodeLength;
        g_pEffectPositionNormal->GetVertexShaderBytecode(&shaderByteCode, &byteCodeLength);

        pd3dDevice->CreateInputLayout(VertexPositionNormal::InputElements,
                                      VertexPositionNormal::InputElementCount,
                                      shaderByteCode, byteCodeLength,
                                      &g_pInputLayoutPositionNormal);

        // Primitive batch
        g_pPrimitiveBatchPositionNormal = new PrimitiveBatch<VertexPositionNormal>(pd3dImmediateContext);
    }

    // Create effect, input layout and primitive batch for position/normal/color vertices (DirectXTK)
    {
        // Effect
        g_pEffectPositionNormalColor = new BasicEffect(pd3dDevice);
        g_pEffectPositionNormalColor->SetPerPixelLighting(true);
        g_pEffectPositionNormalColor->EnableDefaultLighting();     // triggers usage of position/normal/color vertices
        g_pEffectPositionNormalColor->SetVertexColorEnabled(true); // triggers usage of position/normal/color vertices

        // Input layout
        void const* shaderByteCode;
        size_t byteCodeLength;
        g_pEffectPositionNormalColor->GetVertexShaderBytecode(&shaderByteCode, &byteCodeLength);

        pd3dDevice->CreateInputLayout(VertexPositionNormalColor::InputElements,
                                      VertexPositionNormalColor::InputElementCount,
                                      shaderByteCode, byteCodeLength,
                                      &g_pInputLayoutPositionNormalColor);

        // Primitive batch
        g_pPrimitiveBatchPositionNormalColor = new PrimitiveBatch<VertexPositionNormalColor>(pd3dImmediateContext);
    }

	// create rigid bodies
	initRigidBodies();

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Release D3D11 resources created in OnD3D11CreateDevice 
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11DestroyDevice( void* pUserContext )
{
	SAFE_RELEASE(g_pEffect);
	
    TwDeleteBar(g_pTweakBar);
    g_pTweakBar = nullptr;
	TwTerminate();

    g_pSphere.reset();
	g_pCube.reset();
    g_pTeapot.reset();
    
    SAFE_DELETE (g_pPrimitiveBatchPositionColor);
    SAFE_RELEASE(g_pInputLayoutPositionColor);
    SAFE_DELETE (g_pEffectPositionColor);

    SAFE_DELETE (g_pPrimitiveBatchPositionNormal);
    SAFE_RELEASE(g_pInputLayoutPositionNormal);
    SAFE_DELETE (g_pEffectPositionNormal);

    SAFE_DELETE (g_pPrimitiveBatchPositionNormalColor);
    SAFE_RELEASE(g_pInputLayoutPositionNormalColor);
    SAFE_DELETE (g_pEffectPositionNormalColor);
}

//--------------------------------------------------------------------------------------
// Create any D3D11 resources that depend on the back buffer
//--------------------------------------------------------------------------------------
HRESULT CALLBACK OnD3D11ResizedSwapChain( ID3D11Device* pd3dDevice, IDXGISwapChain* pSwapChain,
                                          const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc, void* pUserContext )
{
    // Update camera parameters
	int width = pBackBufferSurfaceDesc->Width;
	int height = pBackBufferSurfaceDesc->Height;
	g_camera.SetWindow(width, height);
g_camera.SetProjParams(XM_PI / 4.0f, float(width) / float(height), 0.1f, 100.0f);

// Inform AntTweakBar about back buffer resolution change
TwWindowSize(pBackBufferSurfaceDesc->Width, pBackBufferSurfaceDesc->Height);

return S_OK;
}

//--------------------------------------------------------------------------------------
// Release D3D11 resources created in OnD3D11ResizedSwapChain 
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11ReleasingSwapChain(void* pUserContext)
{
}

//--------------------------------------------------------------------------------------
// Handle key presses
//--------------------------------------------------------------------------------------
void CALLBACK OnKeyboard(UINT nChar, bool bKeyDown, bool bAltDown, void* pUserContext)
{
	HRESULT hr;

	if (bKeyDown)
	{
		keyDownMap[nChar] = true;
		int toBeToggled = -2; // welches Element eingef�gt bzw. entfernt werden soll
		switch (nChar)
		{
			// RETURN: toggle fullscreen
		case VK_RETURN:
		{
			if (bAltDown) DXUTToggleFullScreen();
			break;
		}
			// F8: Take screenshot
		case VK_F8:
		{
			// Save current render target as png
			static int nr = 0;
			std::wstringstream ss;
			ss << L"Screenshot" << std::setfill(L'0') << std::setw(4) << nr++ << L".png";

			ID3D11Resource* pTex2D = nullptr;
			DXUTGetD3D11RenderTargetView()->GetResource(&pTex2D);
			SaveWICTextureToFile(DXUTGetD3D11DeviceContext(), pTex2D, GUID_ContainerFormatPng, ss.str().c_str());
			SAFE_RELEASE(pTex2D);

			std::wcout << L"Screenshot written to " << ss.str() << std::endl;
			break;
		}
			// F10: Toggle video recording
		case VK_F10:
		{
			if (!g_pFFmpegVideoRecorder) {
				g_pFFmpegVideoRecorder = new FFmpeg(25, 21, FFmpeg::MODE_INTERPOLATE);
				V(g_pFFmpegVideoRecorder->StartRecording(DXUTGetD3D11Device(), DXUTGetD3D11RenderTargetView(), "output.avi"));
			}
			else {
				g_pFFmpegVideoRecorder->StopRecording();
				SAFE_DELETE(g_pFFmpegVideoRecorder);
			}
			break;
		}
		case 0x30: // 0
		case 0x31: // 1
		case 0x32: // 2
		case 0x33: // 3
		case 0x34: // 4
		case 0x35: // 5
		case 0x36: // 6
		case 0x37: // 7
		case 0x38: // 8
		case 0x39: // 9
		{
			toBeToggled = nChar - 0x30 - 1;
			break;
		}
		case VK_NUMPAD0: // 0
		case VK_NUMPAD1: // 1
		case VK_NUMPAD2: // 2
		case VK_NUMPAD3: // 3
		case VK_NUMPAD4: // 4
		case VK_NUMPAD5: // 5
		case VK_NUMPAD6: // 6
		case VK_NUMPAD7: // 7
		case VK_NUMPAD8: // 8
		case VK_NUMPAD9: // 9
		{
			toBeToggled = nChar - VK_NUMPAD0 - 1;
		}
		} // end of switch
		if (toBeToggled > -2){ // nur wenn eine Taste gedr�ckt wurde
			if (toBeToggled == -1){
				bool alleSelected = true;
				for (std::vector<RigidBody>::iterator it = rigidBodies.begin(); it != rigidBodies.end(); it++){
					if (!it->selected){ // wenn einer nicht selected ist, selecte alle.
						alleSelected = false;
						it->selected = true;
					}
				}
				if (alleSelected){ // wenn alle selected sind, dann ent-selecte alle
					for (std::vector<RigidBody>::iterator it = rigidBodies.begin(); it != rigidBodies.end(); it++){
						it->selected = false;
					}
				}
			}
			else if (toBeToggled < rigidBodies.size()){
				rigidBodies[toBeToggled].selected = !rigidBodies[toBeToggled].selected;
			}
		}
	}
	else{
		keyDownMap[nChar] = false;
	}
}


//--------------------------------------------------------------------------------------
// Handle mouse button presses
//--------------------------------------------------------------------------------------
void CALLBACK OnMouse( bool bLeftButtonDown, bool bRightButtonDown, bool bMiddleButtonDown,
                       bool bSideButton1Down, bool bSideButton2Down, int nMouseWheelDelta,
                       int xPos, int yPos, void* pUserContext )
{
    // Track mouse movement if left mouse key is pressed
    {
        static int xPosSave = 0, yPosSave = 0;

        if (bLeftButtonDown)
        {
            // Accumulate deltas in g_viMouseDelta
            g_viMouseDelta.x += xPos - xPosSave;
            g_viMouseDelta.y += yPos - yPosSave;

        }    

        xPosSave = xPos;
        yPosSave = yPos;
    }
}


//--------------------------------------------------------------------------------------
// Handle messages to the application
//--------------------------------------------------------------------------------------
LRESULT CALLBACK MsgProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam,
                          bool* pbNoFurtherProcessing, void* pUserContext )
{
    // Send message to AntTweakbar first
    if (TwEventWin(hWnd, uMsg, wParam, lParam))
    {
        *pbNoFurtherProcessing = true;
        return 0;
    }

    // If message not processed yet, send to camera
	if(g_camera.HandleMessages(hWnd,uMsg,wParam,lParam))
    {
        *pbNoFurtherProcessing = true;
		return 0;
    }

	return 0;
}


//--------------------------------------------------------------------------------------
// Handle updates to the scene
//--------------------------------------------------------------------------------------
void CALLBACK OnFrameMove( double dTime, float fElapsedTime, void* pUserContext )
{
	UpdateWindowTitle(L"Demo");

    // Move camera
    g_camera.FrameMove(fElapsedTime);

    // Update effects with new view + proj transformations
    g_pEffectPositionColor      ->SetView      (g_camera.GetViewMatrix());
    g_pEffectPositionColor      ->SetProjection(g_camera.GetProjMatrix());

    g_pEffectPositionNormal     ->SetView      (g_camera.GetViewMatrix());
    g_pEffectPositionNormal     ->SetProjection(g_camera.GetProjMatrix());

    g_pEffectPositionNormalColor->SetView      (g_camera.GetViewMatrix());
    g_pEffectPositionNormalColor->SetProjection(g_camera.GetProjMatrix());

    // Apply accumulated mouse deltas to g_vfMovableObjectPos (move along cameras view plane)
    if (g_viMouseDelta.x != 0 || g_viMouseDelta.y != 0)
    {
        // Calcuate camera directions in world space
        XMMATRIX viewInv = XMMatrixInverse(nullptr, g_camera.GetViewMatrix());
        XMVECTOR camRightWorld = XMVector3TransformNormal(g_XMIdentityR0, viewInv);
        XMVECTOR camUpWorld    = XMVector3TransformNormal(g_XMIdentityR1, viewInv);

        // Add accumulated mouse deltas to movable object pos
        XMVECTOR vMovableObjectPos = XMLoadFloat3(&g_vfMovableObjectPos);

        float speedScale = 0.001f;
        vMovableObjectPos = XMVectorAdd(vMovableObjectPos,  speedScale * (float)g_viMouseDelta.x * camRightWorld);
        vMovableObjectPos = XMVectorAdd(vMovableObjectPos, -speedScale * (float)g_viMouseDelta.y * camUpWorld);

        XMStoreFloat3(&g_vfMovableObjectPos, vMovableObjectPos);
        
        // Reset accumulated mouse deltas
        g_viMouseDelta = XMINT2(0,0);
    }

	// keyboard input:
	float impulsStrength = 20;
	for (std::vector<RigidBody>::iterator it = rigidBodies.begin(); it != rigidBodies.end(); it++){
		if (it->selected){
			if (keyDownMap[0x44]) // D
			{
				it->userInputForce.x += impulsStrength;
			}
			if (keyDownMap[0x41]) // A
			{
				it->userInputForce.x -= impulsStrength;
			}
			if (keyDownMap[0x45]) // E
			{
				it->userInputForce.y += impulsStrength;
			}
			if (keyDownMap[0x51]) // Q
			{
				it->userInputForce.y -= impulsStrength;
			}
			if (keyDownMap[0x57]) // W
			{
				it->userInputForce.z += impulsStrength;
			}
			if (keyDownMap[0x53]) // S
			{
				it->userInputForce.z -= impulsStrength;
			}
		}
	}
	
	std::vector<XMFLOAT3> forces;
	forces.push_back(XMFLOAT3(0, -g_fGravity, 0)); // gravity
	// simulation algorithm 3d:
	for(int i = 0; i < rigidBodies.size(); i++){
		rigidBodies[i].simulationStep(fElapsedTime, forces);
	}

	// collision detection:
	for(int i = 0; i < rigidBodies.size(); i++){
		XMMATRIX scale1 = XMMatrixScaling(rigidBodies[i].seitenlaenge, rigidBodies[i].seitenlaenge, rigidBodies[i].seitenlaenge);
		XMMATRIX trans1 = XMLoadFloat4x4(&rigidBodies[i].transform);
		XMMATRIX tmp1   = trans1;
		for(int j = 0; j < i; j++){
			XMMATRIX scale2 = XMMatrixScaling(rigidBodies[j].seitenlaenge, rigidBodies[j].seitenlaenge, rigidBodies[j].seitenlaenge);
			XMMATRIX trans2 = XMLoadFloat4x4(&rigidBodies[j].transform);
			XMMATRIX tmp2   = trans2;
			CollisionInfo info = checkCollision(tmp1, tmp2, 2*rigidBodies[i].seitenlaenge, 2*rigidBodies[i].seitenlaenge, 2*rigidBodies[i].seitenlaenge, 2*rigidBodies[j].seitenlaenge, 2*rigidBodies[j].seitenlaenge, 2*rigidBodies[j].seitenlaenge);
			if(info.isValid){
				float c = 0.2f; // bounciness
				XMVECTOR v_rel = XMLoadFloat3(&rigidBodies[i].velocity) - XMLoadFloat3(&rigidBodies[j].velocity);
				XMVECTOR teil1, teil2;
				// f�r w�rfel a:
				teil1 = XMVector3Transform(XMVector3Cross(XMLoadFloat3(&rigidBodies[i].position), info.normalWorld), XMLoadFloat3x3(&rigidBodies[i].inertiaTensorInverse) ); // I^-1 * (pos x n)
				teil1 = XMVector3Cross(teil1, XMLoadFloat3(&rigidBodies[i].position)); // (I^-1 * (pos x n)) x pos
				// f�r w�rfel b:
				teil2 = XMVector3Transform(XMVector3Cross(XMLoadFloat3(&rigidBodies[j].position), info.normalWorld), XMLoadFloat3x3(&rigidBodies[j].inertiaTensorInverse) ); // I^-1 * (pos x n)
				teil2 = XMVector3Cross(teil2, XMLoadFloat3(&rigidBodies[j].position)); // (I^-1 * (pos x n)) x pos
				XMFLOAT3 dotNenner, dotZaehler; // zum berechnen der Skalarprodukte in der Formel
				XMStoreFloat3(&dotNenner, XMVector3Dot((teil1 + teil2), info.normalWorld)); // in jeder Komponente steht der selbe Wert des Skalarprodukts. (teil1 + teil2) * n
				XMStoreFloat3(&dotZaehler, XMVector3Dot(v_rel, info.normalWorld)); // v_rel * n
				float jacobi = -(1.0f+c) * dotZaehler.x / (1.0f/rigidBodies[i].mass + 1.0f/rigidBodies[j].mass + dotNenner.x);
				XMFLOAT3 normalTmp;
				XMStoreFloat3(&normalTmp, info.normalWorld);
				XMFLOAT3 tmpppp;
				XMStoreFloat3(&tmpppp, info.collisionPointWorld);
				std::printf("%f, %f, %f\n", tmpppp.x, tmpppp.y, tmpppp.z);
				// update velocity:
				rigidBodies[i].velocity.x += jacobi * normalTmp.x / rigidBodies[i].mass;
				rigidBodies[i].velocity.y += jacobi * normalTmp.y / rigidBodies[i].mass;
				rigidBodies[i].velocity.z += jacobi * normalTmp.z / rigidBodies[i].mass;

				rigidBodies[j].velocity.x -= jacobi * normalTmp.x / rigidBodies[j].mass;
				rigidBodies[j].velocity.y -= jacobi * normalTmp.y / rigidBodies[j].mass;
				rigidBodies[j].velocity.z -= jacobi * normalTmp.z / rigidBodies[j].mass;
				// update drehimpuls
				XMFLOAT3 posKreuzJn;
				XMStoreFloat3(&posKreuzJn, XMVector3Cross(XMLoadFloat3(&rigidBodies[i].position), jacobi*info.normalWorld)); // pos x J*n
				rigidBodies[i].angularMomentum.x += posKreuzJn.x;
				rigidBodies[i].angularMomentum.y += posKreuzJn.y;
				rigidBodies[i].angularMomentum.z += posKreuzJn.z;
				
				XMStoreFloat3(&posKreuzJn, XMVector3Cross(XMLoadFloat3(&rigidBodies[j].position), jacobi*info.normalWorld)); // pos x J*n
				rigidBodies[j].angularMomentum.x -= posKreuzJn.x;
				rigidBodies[j].angularMomentum.y -= posKreuzJn.y;
				rigidBodies[j].angularMomentum.z -= posKreuzJn.z;
			}
		}
	}
}

//--------------------------------------------------------------------------------------
// Render the scene using the D3D11 device
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11FrameRender( ID3D11Device* pd3dDevice, ID3D11DeviceContext* pd3dImmediateContext,
                                  double fTime, float fElapsedTime, void* pUserContext )
{
    HRESULT hr;

	// Clear render target and depth stencil
	float ClearColor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };

	ID3D11RenderTargetView* pRTV = DXUTGetD3D11RenderTargetView();
	ID3D11DepthStencilView* pDSV = DXUTGetD3D11DepthStencilView();
	pd3dImmediateContext->ClearRenderTargetView( pRTV, ClearColor );
	pd3dImmediateContext->ClearDepthStencilView( pDSV, D3D11_CLEAR_DEPTH, 1.0f, 0 );

    // Draw floor
    DrawFloor(pd3dImmediateContext);

    // Draw axis box
    //DrawBoundingBox(pd3dImmediateContext);

    // Draw speheres
    //if (g_bDrawSpheres) DrawSomeRandomObjects(pd3dImmediateContext);

    // Draw movable teapot
    if (g_bDrawTeapot) DrawMovableTeapot(pd3dImmediateContext);

    // Draw simple triangle
    if (g_bDrawTriangle) DrawTriangle(pd3dImmediateContext);

	// Draw rigid bodies
	DrawRigidBodies(pd3dImmediateContext);

    // Draw GUI
    TwDraw();

    if (g_pFFmpegVideoRecorder) 
    {
        V(g_pFFmpegVideoRecorder->AddFrame(pd3dImmediateContext, DXUTGetD3D11RenderTargetView()));
    }
}

//--------------------------------------------------------------------------------------
// Initialize everything and go into a render loop
//--------------------------------------------------------------------------------------
int main(int argc, char* argv[])
{
#if defined(DEBUG) | defined(_DEBUG)
	// Enable run-time memory check for debug builds.
	// (on program exit, memory leaks are printed to Visual Studio's Output console)
	_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif

#ifdef _DEBUG
	std::wcout << L"---- DEBUG BUILD ----\n\n";
#endif

	// Set general DXUT callbacks
	DXUTSetCallbackMsgProc( MsgProc );
	DXUTSetCallbackMouse( OnMouse, true );
	DXUTSetCallbackKeyboard( OnKeyboard );

	DXUTSetCallbackFrameMove( OnFrameMove );
	DXUTSetCallbackDeviceChanging( ModifyDeviceSettings );

	// Set the D3D11 DXUT callbacks
	DXUTSetCallbackD3D11DeviceAcceptable( IsD3D11DeviceAcceptable );
	DXUTSetCallbackD3D11DeviceCreated( OnD3D11CreateDevice );
	DXUTSetCallbackD3D11SwapChainResized( OnD3D11ResizedSwapChain );
	DXUTSetCallbackD3D11FrameRender( OnD3D11FrameRender );
	DXUTSetCallbackD3D11SwapChainReleasing( OnD3D11ReleasingSwapChain );
	DXUTSetCallbackD3D11DeviceDestroyed( OnD3D11DestroyDevice );

	// Init keyboard map:
	for (int i = 0; i < 256; i++){
		keyDownMap[i] = false;
	}

    // Init camera
 	XMFLOAT3 eye(0.0f, 0.0f, -4.0f);
	XMFLOAT3 lookAt(0.0f, 0.0f, 0.0f);
	g_camera.SetViewParams(XMLoadFloat3(&eye), XMLoadFloat3(&lookAt));
    g_camera.SetButtonMasks(0, MOUSE_WHEEL, MOUSE_RIGHT_BUTTON);

    // Init DXUT and create device
	DXUTInit( true, true, NULL ); // Parse the command line, show msgboxes on error, no extra command line params
	//DXUTSetIsInGammaCorrectMode( false ); // true by default (SRGB backbuffer), disable to force a RGB backbuffer
	DXUTSetCursorSettings( true, true ); // Show the cursor and clip it when in full screen
	DXUTCreateWindow( L"Demo" );
	DXUTCreateDevice( D3D_FEATURE_LEVEL_10_0, true, 1280, 960 );
    
	DXUTMainLoop(); // Enter into the DXUT render loop

	DXUTShutdown(); // Shuts down DXUT (includes calls to OnD3D11ReleasingSwapChain() and OnD3D11DestroyDevice())

	return DXUTGetExitCode();
}
