#include <vector>

#include <DirectXMath.h>
using namespace DirectX; 

#pragma once
class RigidBody
{
public:
	XMVECTOR color;
	float seitenlaenge; // s
	float mass; // M
	float bounciness;
	bool selected;
	XMFLOAT3 userInputForce; // wenn man auf WAESRD dr�ckt, wird eine einmalige Kraft aufge�bt.

	XMFLOAT3 position; // x_cm
	XMFLOAT3 worldPosition; // x^world
	XMFLOAT3 velocity; // v
	XMFLOAT3 worldVelocity; // v^world
	XMFLOAT3X3 inertiaTensorInverse; // Tr�gheitstensor: I^-1
	XMVECTOR orientation; // r (4 Zeilen)
	XMFLOAT3 angularVelocity; // Winkelgeschwindigkeit: w
	XMFLOAT3 angularMomentum; // Drehimpuls L

	XMFLOAT3 forceAccumulator; // Summe aller Kr�fte f_i: F
	XMFLOAT3 torqueAccumulator; // Summe aller Drehmomente q_i: q

	XMFLOAT4X4 transform; // (4x4 Matrix)

	XMFLOAT3X3 inertiaTensorInverseInitial; // I_0^-1


	RigidBody(float x, float y, float z, float s, float b, XMVECTOR c, XMVECTOR initialRotationAxis, float initialRotationAngle);
	~RigidBody();

	void simulationStep(float h, std::vector<XMFLOAT3> forces);
};

