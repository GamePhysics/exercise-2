#include "RigidBody.h"

/*
Konvertierung 
XMFLOAT3 -> XMVECTOR: xmVector = XMLoadFloat3(&xmfloat3);
XMVECTOR -> XMFLOAT3: XMStoreFloat3(&xmfloat3, xmVector);
*/


// �bergibt Position und Seitenl�nge und bounciness und Farbe des W�rfels
RigidBody::RigidBody(float x, float y, float z, float s, float b, XMVECTOR c, XMVECTOR initialRotationAxis, float initialRotationAngle)
{
	seitenlaenge = s;
	bounciness = b;
	color = c;
	mass = 1.5f;
	userInputForce = XMFLOAT3(0, 0, 0);
	selected = false;
	position = XMFLOAT3(x + s / 2.0f, y + s / 2.0f, z + s / 2.0f); // center of mass berechnen
	worldPosition = position;
	velocity = XMFLOAT3(0, 0, 0);
	worldVelocity = XMFLOAT3(0, 0, 0);
	inertiaTensorInverseInitial = XMFLOAT3X3( // I_0^-1
		6.0f / (mass*seitenlaenge*seitenlaenge), 0, 0,
		0, 6.0f / (mass*seitenlaenge*seitenlaenge), 0,
		0, 0, 6.0f / (mass*seitenlaenge*seitenlaenge)
		);

	XMStoreFloat4x4(&transform, XMMatrixRotationAxis(initialRotationAxis, initialRotationAngle));//XMMatrixRotationY(XM_PIDIV4)); // initiale orientierung des W�rfels setzen
	orientation = XMQuaternionRotationMatrix(XMLoadFloat4x4(&transform)); // und in Quaternion umwandeln
	angularMomentum = XMFLOAT3(0, 0, 0);

	XMStoreFloat3x3(&inertiaTensorInverse, 
		XMMatrixMultiply(XMMatrixMultiply( XMLoadFloat4x4(&transform), XMLoadFloat3x3(&inertiaTensorInverseInitial) ),
		XMMatrixTranspose(XMLoadFloat4x4(&transform))
		)
		); // I^-1 = Rot * I_0^-1 * Rot^T

	XMStoreFloat3(&angularVelocity,
		XMVector3Transform(XMLoadFloat3(&angularMomentum), XMLoadFloat3x3(&inertiaTensorInverse))
		); // w = I^-1 * L
}


RigidBody::~RigidBody()
{
}

// simulation algorithm 3d:
// forces: list of f_i
void RigidBody::simulationStep(float h, std::vector<XMFLOAT3> forces){
	XMVECTOR torqueAccTmp = XMVectorSet(0, 0, 0, 0);
	forceAccumulator.x = userInputForce.x;
	forceAccumulator.y = userInputForce.y;
	forceAccumulator.z = userInputForce.z;
	torqueAccTmp = XMVectorAdd(torqueAccTmp, XMVector3Cross(XMLoadFloat3(&position), XMLoadFloat3(&userInputForce)));
	userInputForce.x = 0;
	userInputForce.y = 0;
	userInputForce.z = 0;
	for (std::vector<XMFLOAT3>::size_type i = 0; i < forces.size(); i++){
		// external forces F = sum over f_i:
		forceAccumulator.x += forces[i].x;
		forceAccumulator.y += forces[i].y;
		forceAccumulator.z += forces[i].z;
		// Drehmoment (torque) q = sum over x_i x f_i:
		torqueAccTmp = XMVectorAdd(torqueAccTmp, XMVector3Cross( XMLoadFloat3(&position), XMLoadFloat3(&forces[i]) ));
	}
	XMStoreFloat3(&torqueAccumulator, torqueAccTmp); // store q

	// Euler step:
	// x_cm = x_cm + h*v_cm
	position.x += h * velocity.x;
	position.y += h * velocity.y;
	position.z += h * velocity.z;

	// v_cm = v_cm + h*F/M
	velocity.x += h*forceAccumulator.x / mass;
	velocity.y += h*forceAccumulator.y / mass;
	velocity.z += h*forceAccumulator.z / mass;

	//Geschwindigkeit d�mpfen

	velocity.x -= 0.005 * velocity.x;
	velocity.y -= 0.0005 * velocity.y;
	velocity.z -= 0.005 * velocity.z;

	if (position.y < -1 + seitenlaenge){
		position.y = -1 + seitenlaenge + 0.0000000000001f;
		velocity.y *= -bounciness;
	}

	// Rotation:
	// r = r + h*w
	orientation = orientation + h*XMLoadFloat3(&angularVelocity);

	// Drehimpuls (angular momentum L):
	// L = L + h*q
	angularMomentum.x += h*torqueAccumulator.x;
	angularMomentum.y += h*torqueAccumulator.y;
	angularMomentum.z += h*torqueAccumulator.z;

	// Tr�gheitstensor (I^-1):
	// I^-1 = Rot * I_0^-1 * Rot^T
	XMStoreFloat3x3(&inertiaTensorInverse,
		XMMatrixMultiply(XMMatrixMultiply( XMLoadFloat4x4(&transform), XMLoadFloat3x3(&inertiaTensorInverseInitial) ),
		XMMatrixTranspose(XMLoadFloat4x4(&transform))
		)
		);

	// Winkelgeschwindigkeit (angular velocity w):
	// w = i^-1 * L
	XMStoreFloat3(&angularVelocity,
		XMVector3Transform(XMLoadFloat3(&angularMomentum), XMLoadFloat3x3(&inertiaTensorInverse))
		);

	// World Position berechnen:
	// x_i^world = quaternion * x_i * quaternion^-1
	XMVECTOR worldTmp = XMQuaternionMultiply(orientation, XMLoadFloat3(&position));
	worldTmp = XMQuaternionMultiply(worldTmp, XMQuaternionInverse(orientation));
	XMStoreFloat3(&worldPosition, worldTmp);

	// normalize quaternion:
	orientation = XMQuaternionNormalize(orientation);

	// Transform-Matrix aufbauen:
	XMStoreFloat4x4(&transform, XMMatrixRotationQuaternion(orientation));
	transform._41 = position.x;
	transform._42 = position.y;
	transform._43 = position.z;
	transform._14 = 0;
	transform._24 = 0;
	transform._34 = 0;
	transform._44 = 1;

	// x_i^world = x_cm + Rot * x_i
	/*XMFLOAT3 rotApplied;
	XMStoreFloat3(&rotApplied,
	XMVector3Transform(XMLoadFloat3(&position), XMLoadFloat4x4(&transform))
	);
	worldPosition.x = position.x +rotApplied.x;
	worldPosition.y = position.y +rotApplied.y;
	worldPosition.z = position.z +rotApplied.z;

	// v_i^world = v_cm + w x x_i
	XMFLOAT3 velApplied;
	XMStoreFloat3(&velApplied,
	XMVector3Cross(XMLoadFloat3(&angularVelocity), XMLoadFloat3(&position))
	);
	worldVelocity.x = velocity.x + velApplied.x;
	worldVelocity.y = velocity.y + velApplied.y;
	worldVelocity.z = velocity.z + velApplied.z;*/
}
